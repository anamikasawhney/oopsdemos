﻿using System.Runtime.InteropServices.Marshalling;
using System.Security.Cryptography;
using System.Windows.Markup;

namespace ArrayOperations
{
    internal class Program
    {
        static int[] num = new int[] { 3, 4, 10, 20, 21, 23, 78 ,0,0,0 };
        static void Main(string[] args)
        {
            string choice = "y";
            while (choice.ToLower() =="y")
            {
                int ch = Menu();
                switch (ch)
                {
                    case 1:
                        {
                            Console.WriteLine("Enyter element to insert");
                            int x = byte.Parse(Console.ReadLine());
                            InsertElement(x);
                            break;
                        }
                    case 2:
                        {
                            Console.WriteLine("Enyter element to delete");
                            int x = byte.Parse(Console.ReadLine());
                            DeleteElement(x);
                            break;

                        }
                    case 3:
                        {
                            PrintElements();
                            break;
                        }
                    case 4:
                        {
                            Console.WriteLine(  "Enter the index from where to get element ");
                            int x= byte.Parse(Console.ReadLine());
                            int data = ReturnElement(x);
                            Console.WriteLine($"Element at {x} is {data}");
                            break;
                        }
                    case 5:
                        {
                            int[] newArray = SliceArray(num, 3, 6);
                            foreach(int x in newArray)
                                Console.WriteLine(x);
                            break;
                        }
                }
                Console.WriteLine("Repeat?");
                choice = Console.ReadLine();
            }
            }

        private static int Menu()
        {
            Console.WriteLine("Main Menu");
            Console.WriteLine("1. Inserty Element");
            Console.WriteLine("2.Delete ELement");
            Console.WriteLine("3. List Elements");
            Console.WriteLine("4. Get Element at some index");
            Console.WriteLine("5. Copy Array/Get Slice of Array in other Array");
            Console.WriteLine("What ypu need to perform");
            int ch = byte.Parse(Console.ReadLine());
            return ch;
        }
        static int[] SliceArray(int[] num, int start, int last)
        {
            int[] newArray = new int[10];
            Array.Copy(num, start, newArray, 0, (last - start)+1);
           
            return newArray;
        }
        static int GetPositionOf0()
        {
            int pos = 0;
            for(int i= 0;i < num.Length;i++)
            {
                if (num[i] == 0)

                {
                    pos = i;
                    break;
                }
            }
            return pos;
        }
        static int InsertElement(int x)
        {
            int i = 0;
            int pos = GetPositionOf0();
            if (x <= num[0])
            {
                for(i = pos;i>=0; i--)
                {
                    num[i+1] = num[i]; 
                }
                num[0] = x;
                return 0;
            }
            else if(x > num[pos-1])
            {
                num[pos] = x;

                return pos;
            }
            else
            {
                for(i = 0;i< num.Length;i++)
                {
                    if(x >= num[i] && x < num[i+1])
                    { 
                      for(int j = pos;j>i;j--)
                        {
                            num[j+1] = num[j];
                        }
                        num[i+1] = x;
                        break;
                    }
                   
                }
                return i;
            }
             

        }

        static void PrintElements()
        {
            foreach(int x in num)
                Console.WriteLine(x);
        }

        static int ReturnElement(int x)
        {
            return num[x];
        }
        static void DeleteElement(int x)
        {
           for(int i = 0;i<num.Length;i++)
            {
                if(x == num[i])
                {
                    for(int j = i;j<GetPositionOf0();j++)
                    {
                        num[j] = num[j + 1];
                    }
                                    }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayOperations
{
    internal class StringFunctions
    {
        static void Main()
        {
            string sentnece = "This is my. book of C# No 909078#";
            Console.WriteLine(  sentnece.ToLower());
            Console.WriteLine(  sentnece.ToUpper());
            Console.WriteLine(  "No of chgaracters " + sentnece.Length);
            // Get no of words in sentence
            //string[] words = sentnece.Split(' ');
            string[] words = sentnece.Split(new char[] { ' ', '.' });
            Console.WriteLine( words.Length);

            int vowel = 0;
            int con = 0;
            int digits = 0;
            int sp = 0;
            // coungt no of vowels / cons / inte
           for(int i = 0;i<sentnece.Length;i++)
            {
                if (char.IsDigit(sentnece[i]))
                {
                    digits++;

                }
                else if (char.IsLetter(sentnece[i]))
                {
                    if (sentnece[i] == 'a')
                        vowel++;
                    else
                        con++;
                }
                else
                    sp++;
            }

            Console.WriteLine(sentnece.Contains('a'));
            Console.WriteLine(sentnece.IndexOf('a'));
            Console.WriteLine( sentnece.Reverse());
            Console.WriteLine(  sentnece.Substring(3));
            Console.WriteLine( sentnece.Substring(2,10));
            Console.WriteLine(  sentnece.Remove(6));
            Console.WriteLine(  sentnece.Replace('b','B'));


        }
    }
}

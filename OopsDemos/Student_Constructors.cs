﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OopsDemos1
{
    
    internal class Student
    {
        int rn;
        string name;
        static  string batch;
        string collegeName;
        int marks;
        public Student() // def const
        {
                
        }
        public Student(int rn)  // para const
        {
            this.rn = rn;
            Console.WriteLine("Enter Name");
            name = Console.ReadLine();
            Console.WriteLine("Enter BAtch Name");
            batch = Console.ReadLine();
            Console.WriteLine("Enter collegeName");
            collegeName = Console.ReadLine();
            Console.WriteLine("Enter marks ");
            marks = byte.Parse(Console.ReadLine());

        }
        public Student(int rn, string name)  // para const
        {
            this.rn = rn;

            this.name = name;
            Console.WriteLine("Enter BAtch Name");
            batch = Console.ReadLine();
            Console.WriteLine("Enter collegeName");
            collegeName = Console.ReadLine();
            Console.WriteLine("Enter marks ");
            marks = byte.Parse(Console.ReadLine());

        }
        public Student(int rn, string name, string collegeName,
            int marks)  // fully para const
        {
            this.rn = rn;
            this.name = name;
            //this.batch = batch;
            this.collegeName = collegeName;
            this.marks = marks;
        }

        public Student(Student student)  // copy const
        {
            Console.WriteLine("Enter RollNo");
            rn = byte.Parse(Console.ReadLine());
            Console.WriteLine("Enter Name");
            name = Console.ReadLine();
            //this.batch = student.batch;
            this.collegeName = student.collegeName;
            this.marks = student.marks;
        }

        static Student() // static constrcutor
        {
            batch = "B001";
        }
        public void GetDetails()
        {
            Console.WriteLine("Enter RollNo");
            rn = byte.Parse(Console.ReadLine());
            Console.WriteLine("Enter Name");
            name = Console.ReadLine();
            Console.WriteLine("Enter BAtch Name");
            batch = Console.ReadLine();
            Console.WriteLine("Enter collegeName");
            collegeName = Console.ReadLine();
            Console.WriteLine("Enter marks ");
            marks = byte.Parse(Console.ReadLine()); 
        }
        public void DisplayDetails()
        {
            Console.WriteLine($"Roll No is {rn}");
            Console.WriteLine($"NAme is {name}");
            Console.WriteLine($"NBatch is {batch}");
            Console.WriteLine($"collegeName is {collegeName}");
            Console.WriteLine($"Marks are {marks}");
        }

    }
}

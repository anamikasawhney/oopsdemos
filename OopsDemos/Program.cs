﻿using System.Security;

namespace OopsDemos
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // declaration
            Student student;
            // initialization
            student = new Student();
            // new keyword will do 2 things
            // 1. It calls/invokes constructor,  It initializes all the variables 
            // 2. it allocates memory from heap
            //Student student2 = new Student();
            //student2.GetDetails();
            //student2.DisplayDetails();
            //Student stud1 = new Student();
            //Student stud2 = new Student();
            //Student stud3 = new Student();
            // Array of Objects
            //Student[] students = new Student[10];
            //for(int i = 0;i<5;i++)
            //{
            //    Console.WriteLine("Enter Details of student no " + (i+1));
            //    students[i] = new Student();
            //    students[i].GetDetails();   
            //}
            //Console.WriteLine( "Details are ");
            //for (int i = 0; i < 5; i++)
            //{
                 
            //    students[i].DisplayDetails();
            //}

            //List<Student> students= new List<Student>();
            //Student student1 = new Student();
            //student1.GetDetails();
            //students.Add(student1);
            //Student student2 = new Student();
            //student2.GetDetails();
            //students.Add(student2);

            List<Student> students = new List<Student>();
            string ch = "y";
            Student temp;
            while (ch=="y")
            {
               temp = new Student();
                temp.GetDetails();
                students.Add(temp);
                Console.WriteLine("Enat to add more record");
                ch = Console.ReadLine();
            }

            Console.WriteLine("DISPLAYING DETIALS OF " + Student.CollegeName);
            //Student.Batch = "PCAT C# 2";
            Student.BatchDetails();
            //Console.WriteLine("DISPLAYING DETIALS OF Batch " + Student.Batch);

            foreach (Student obj in students)
            {
                obj.DisplayDetails();
            }


        }
    }
}

﻿using System.Security;
using OopsDemos1;
namespace OopsDemos
{
    internal class Program1
    {
        static void Main(string[] args)
        {
              
            OopsDemos1.Student student = new OopsDemos1.Student();
            // new keyword invokes constrctror
            student.GetDetails();

            OopsDemos1.Student student1 = new OopsDemos1.Student(10);

            student1.DisplayDetails();
            OopsDemos1.Student student2 = new OopsDemos1.Student(11, "ajay");
            student2.DisplayDetails();
            OopsDemos1.Student student3 = new OopsDemos1.Student(11, "deepak", "xyz", 90);
            student3.DisplayDetails();

            OopsDemos1.Student student5 = new OopsDemos1.Student(student3); // this will invoke copy const
            student5.DisplayDetails();
        }
    }
}

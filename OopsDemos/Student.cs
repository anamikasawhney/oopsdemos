﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OopsDemos
{
    /// <summary>
    // Access specifiers > How much accessibility we want to give to the members
    // 1. private > by def , class ,members are private, members cant be acessed outside the class
    // 2. public > members are accessible outside the class, also outside the project
    // 3. internal > accessible in the same project/assembly, by def class is internal
    // 4. protected > used in inheritance only
    // 5. internal protected

    // How can we give value to static variable
    //1. While declaring it
    // static string batch = "PCAT C# 2";
    // 2. Make it public , and can access it outside the class
    //3 . Use a static method, make that public

    //variables cud be of 4 types
    // 1. instance variables > are the variables which are different for every instance(object) ,  for eg name, adddrss, mobileNo
    // 2. static variables > are the variables which are common for all objects, there is a soingle copy of this varibale which is shared by all objects, accessible at class level, can be updated
    // 3. Constant varibales > are the variables which are common for all objects, there is a soingle copy of this varibale which is shared by all objects, accessible at class level, can not be updated 
    // 4. readonly variables > are the variables which are different for every instance(object), but once intailized cannot be changed
    // We can give value to readonly variables 1. at time of declaration 2. in a constructor
    /// </summary>
    class Student
    {
        readonly int rn;  // readonly variable
        string name; // instance variable
        static string batch; // static variable
        public const string CollegeName="XYZ";
        int marks;  // instance variable

        public Student()
        {
            Console.WriteLine("Enter RollNo");
            rn = byte.Parse(Console.ReadLine());
        }
        public void GetDetails()
        {
            //Console.WriteLine("Enter RollNo");
            //rn = byte.Parse(Console.ReadLine());
            Console.WriteLine("Enter Name");
            name = Console.ReadLine();
            //Console.WriteLine("Enter BAtch Name");
            //batch = Console.ReadLine();
            //Console.WriteLine("Enter collegeName");
            //collegeName = Console.ReadLine();
            Console.WriteLine("Enter marks ");
            marks = byte.Parse(Console.ReadLine()); 
        }
        public void DisplayDetails()
        {
            Console.WriteLine($"Roll No is {rn}");
            Console.WriteLine($"NAme is {name}");
            //Console.WriteLine($"NBatch is {batch}");
            //Console.WriteLine($"collegeName is {collegeName}");
            Console.WriteLine($"Marks are {marks}");
        }
       public static void BatchDetails()
        { 
            batch = "PCAT C# 2";
            Console.WriteLine("Batch is "  + batch);
        }

    }
}
